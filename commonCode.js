var headHTML = "\
    <link rel='shortcut icon' href='images/favicon.ico'>\
	<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />\
	<meta name='author' content='Firstname Lastname'/>\
	<meta name='robots' content='INDEX,FOLLOW' />\
	<meta name='keywords' content='Firstname Lastname, Firstname,  Lastname, Finance, Economics, Ph.D. Student, PhD Student, JMP, Job Market Paper, University of Chicago, UChicago, U of C, UofC, Chicago Booth, Booth, website, homepage, home page' />\
	<meta name='description' content='Firstname Lastname - PhD Candidate in Economics at the University of Chicago Booth School of Business' />\
	<link href='css/styles.css' rel='stylesheet' type='text/css' media='screen' />";

// Include Google Analytics here
// <meta name='google-site-verification' content='MDcqaXTCKUchHsKh1eGUQsDaBpyrfFmsHSwHyFwJpDA' />\
// (function(i,s,o,g,r,a,m){
//	i['GoogleAnalyticsObject']=r;
//		i[r]=i[r]||function(){
//  			(i[r].q=i[r].q||[]).push(arguments)
//  		},i[r].l=1*new Date();
//  		a=s.createElement(o),m=s.getElementsByTagName(o)[0];
//  		a.async=1;
//  		a.src=g;
//  		m.parentNode.insertBefore(a,m)
//  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');
// ga('create', 'UA-63335516-1', 'auto');
// ga('send', 'pageview');

var topHTML = "\
	<div id='name'>\
		<a href='index.html'><h1> Firstname Lastname </h1> </a>\
		<a href='index.html'><p> PhD Candidate in Economics </p> </a>\
	</div>\
	\
	<div id='logo'>\
		<a href='https://www.chicagobooth.edu' target='_blank'> <img src='images/booth.png' alt='Chicago Booth' id='logo_hover_head'/> </a>\
	</div>";

var leftNavHTML = "\
	<ul>\
		<li><a href='index.html'> HOME </a></li>\
		<li><a href='cv.html'> CV </a></li>\
		<li><a href='research.html'> RESEARCH </a></li>\
		<li><a href='teaching.html'> TEACHING </a></li>\
		<li><a href='references.html'> REFERENCES </a></li>\
		<li><a href='resources.html'> RESOURCES </a></li>\
		<li>\
			<a class='social-icon' href='https://bitbucket.org/' target='_blank'>\
			<img src='images/icon/bitbucket.png' /></a>\
			<a class='social-icon' href='http://www.quora.com/' target='_blank'>\
			<img src='images/icon/quora.png' /></a>\
			<a class='social-icon' href='https://twitter.com/' target='_blank'>\
			<img src='images/icon/twitter.png' /></a>\
		</li>\
	</ul>";

var footerHTML = "\
	<p><a href='index.html' title='Home' target'_self'>Home</a> | <a href='cv.html' title='CV' target'_self'>CV</a> |\
	<a href='research.html' title='Research' target'_self'>Research</a> | <a href='teaching.html' title='Teaching' target'_self'>Teaching</a> |\
	<a href='references.html' title='References' target'_self'>References</a> | <a href='resources.html' title='Resources' target'_self'>Resources</a> </p>\
	<p> <br><font size='2'>&copy; 2017 Firstname Lastname. All rights reserved.</font> </p>\
	<p> <br><font size='2'> based on a <a href = 'https://bitbucket.org/bencharoenwong/website-template'> template</a> provided by <a href = 'https://sites.google.com/view/bencharoenwong'>Ben Charoenwong</a> and Eliot Abrams.</font> </p>\
	<h2><a href='http://www.uchicago.edu/' target='_blank'>\
		<img src='images/uchicago_logo.png' alt='University of Chicago' id='logo_hover_foot'/>\
	</a></h2>";