# README #

This document shows how to pull the files in this repository and edit the html documents to create a functioning academic website with a Home, Teaching, Research, CV, and References page. The template also includes links to the official University of Chicago and Booth School of Business websites.


# Get Started #

To get the website started, follow the three broad steps below.
1. Clone this repository. The "index.html" will be the homepage, so feel free clicking into it and navigate the site.
2. Update supporting documents and images. See below.
3. Upload the documents in the site folder into the server hosting your site, with the default Port = 22 (ensure that the documents have view, write, execute privileges)
    * For UChicago webspace, the host is: webspace.uchicago.edu , and log in with your cNet ID and password.
    * For Chicago Booth PhD site, the host is: phd.chicagobooth.edu , and log in with your Booth ID and password.


## Updating ##

Basic:
1) Replace Firstname, Lastname in all files in "commonHTML"
2) Replace Firstname, Lastname in "index.html," "research.html," "CV.html,"
3) Replace EmailAddress in "index.html
4) Replace PhoneNumber in "index.html
5) Place CV, JMP, WP, etc in "doc"
6) Place profile picture in "images"

As desired:
7) Complete "references.html"
8) Complete "resources.html"
9) Complete "teaching.html"
10) Uncomment out the links to these in "commonHTML/sidNavDiv.hmtl"


# Help #

If any problems arise, feel free to email bcharoen@chicagobooth.edu or eabrams@uchicago.edu. We'd be happy to help should time permit.
